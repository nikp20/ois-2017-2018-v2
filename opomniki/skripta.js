window.addEventListener('load', function() {
	//stran nalozena
	var gumb=document.querySelector("#prijavniGumb");
	gumb.addEventListener("click", function metoda(){
		var vrednost=document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = vrednost;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	});
	
	var gumb2=document.querySelector("#dodajGumb");	
	gumb2.addEventListener("click",function dodajOpomnik(){
		var naziv_opomnika=document.querySelector("#naziv_opomnika").value;
		var cas_opomnika=document.querySelector("#cas_opomnika").value;
		document.querySelector("#cas_opomnika").value="";
		document.querySelector("#naziv_opomnika").value="";
		
		var tekst= "<div class='opomnik'>"+
		"<div class='naziv_opomnika'>" + naziv_opomnika + "</div>"+
		"<div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika + "</span> sekund.</div>"+
		"</div>";
		
		document.querySelector("#opomniki").innerHTML += tekst;
	});
//});
	

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			var imeZadolzitve=opomnik.querySelector(".naziv_opomnika");
			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if(cas==0){

				alert("Opomnik!\n\nZadolzitev "+imeZadolzitve.innerHTML+" je potekla!");
				document.getElementById("opomniki").removeChild(opomnik);
			}
			else{
				cas--;
				//opomnik.querySelector("span").innerHTML=cas+"";
				casovnik.innerHTML=cas + "";
			}
		}
	};
	setInterval(posodobiOpomnike, 1000);

});
